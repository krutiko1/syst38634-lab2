package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalMillisecondsRegular() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:1000A");
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:1000");
		fail("Invalid number of milliseconds");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	
	
	

	


	
	
	
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test
	public void testGetTotalSecondsInBoundary() {
		int totalSeconds = Time.getTotalSeconds("99:99:99");
		assertTrue("The time provided does not match the result", totalSeconds == 362439);
	}
	

	@Test
	public void testGetTotalSecondsOutBoundary() {
		assertThrows(NumberFormatException.class, () -> {Time.getTotalSeconds("100:00:00");});
	}
	
	@Test
	public void testGetTotalSecondsException() {
		assertThrows(StringIndexOutOfBoundsException.class, () -> {Time.getTotalSeconds("9");});
	}
	
	*/

}
